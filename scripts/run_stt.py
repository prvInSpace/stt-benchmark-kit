#!/usr/bin/env python3

import argparse
import hashlib
import json
import os
from pathlib import Path
import subprocess
import sys
from typing import Iterator, Dict, List, Tuple

from helper.output import eprint, print_dictionary
from helper.integrity import calculate_md5
from stt.base import STTModule

class MissingDockerImageException(Exception):
    ...

LINEWIDTH = 45


def main() -> None:
    parser = argparse.ArgumentParser(description="Runs a set of tests using a STT framework")
    parser.add_argument("--lang", choices=os.listdir("data/cv"), required=True, help="The language to train the model on")
    parser.add_argument("--stt", choices=os.listdir("stt-scripts"), required=True, help="The STT framework to use")
    parser.add_argument("--params", nargs="+")
    args = parser.parse_args()

    test_params, static_params = filter_params(args.params)

    md5 = calculate_md5(
        f"data/cv/{args.lang}/train.tsv",
        f"data/cv/{args.lang}/dev.tsv",
        f"data/cv/{args.lang}/test.tsv"
    )

    stt = get_stt_module(args.stt)
    stt.verify_parameters({**test_params, **static_params})

    # Print test params
    if test_params:
        print(f"Parameters to test\n{'='*LINEWIDTH}")
        print_dictionary(test_params)
        print(f"{'='*LINEWIDTH}\n")
    
    # Print any static params
    if static_params:
        print(f"Static parameters\n{'='*LINEWIDTH}")
        print_dictionary(static_params)
        print(f"{'='*LINEWIDTH}\n")

    # Check if Docker image exits
    try:
        create_docker_image(args.stt)
    except MissingDockerImageException as e:
        eprint(e.args[0])

    dir = Path(f"results/{md5[:7]}")#.mkdir(exist_ok=True)
    dir.mkdir(parents=True, exist_ok=True)
    (dir / "checksum").write_text(md5)
    dir: Path = dir / args.stt
    for permutation in parameter_permutations(test_params):
        params = {**permutation, **static_params}
        name = "_".join([ permutation[key] for key in sorted(permutation.keys())])
        if len(name) > 3:
            name = hashlib.md5(json.dumps(permutation).encode("utf8")).hexdigest()[:7]
        settings = dir / name / "hyperparams.json"
        settings.parent.mkdir(parents=True, exist_ok=True)
        settings.write_text(json.dumps(params, indent=2))
        
    for folder in os.listdir(dir):
        checkpoints = dir / folder / "checkpoints" / "checkpoint"
        if True or not checkpoints.exists():
            run_docker(args.stt, dir / folder, args.lang, "train")
        if True or not (dir / folder / "results.json").exists():
            run_docker(args.stt, dir / folder, args.lang, "eval")
            


def run_docker(framework: str, model: Path, lang: str, operation: str):
    print(f"\nTraining model {model}\n{'='*LINEWIDTH}")
    result = subprocess.run(
        ["make", "run", f"FRAMEWORK={framework}", f"MODEL={str(model)}", f"LANG={lang}",
            f"COMMAND=/scripts/{operation}.bash" 
        ],
        cwd=Path(__file__).parent.parent.absolute(),
    )
    if result.returncode != 0:
        exit(result)


def parameter_permutations(parameters: Dict) -> Iterator[Dict[str, str]]:
    """Generates all of the permutations of test parameters
    to train models on.

    Args:
        parameters (Dict): a Dictionary with all of the parameters

    Yields:
        Dict[str, str]: a Dictionary containing a permutation of parameters
    """
    key, values = parameters.popitem()
    for value in values:
        if not len(parameters):
            yield {key: value}
            continue
        for permutation in parameter_permutations(parameters):
            yield {key: value, **permutation}
    parameters[key] = values


def get_stt_module(framework: str) -> STTModule:
    stt = __import__(f"stt.{framework}", fromlist=[framework])
    return stt.get_module()


def create_docker_image(framework: str) -> None:
    """Verifies that a Docker image exists for the given framework.
    Raises an exception if the Docker image can not be found or created.

    Args:
        framework (str): The framework to use
    """
    print(f"Checking that Docker image is up to date.\n{'='*LINEWIDTH}")
    result = subprocess.run(
        ["make", "build", f"FRAMEWORK={framework}"],
        cwd=Path(__file__).parent.parent.absolute(),
    )
    print('='*LINEWIDTH)
    if result.returncode != 0:
        raise MissingDockerImageException(
            f"🛑 Failed to create / verify the existence of a Docker image for {framework}"
        )
    print("✅ Docker image found")


def filter_params(args: List) -> Tuple[Dict, Dict]:
    test_params = dict()
    static_params =dict()

    for param in args:
        if "=" not in param:
            eprint("Parameter '{param}' does not contain a '='")
            sys.exit(2)
        key, value = param.split("=")
        if "," in value:
            test_params[key] = value.split(",")
        else:
            static_params[key] = value
    
    return test_params, static_params


if __name__ == "__main__":
    main()
