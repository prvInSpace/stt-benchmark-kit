
import argparse
from pathlib import Path
import pandas as pd
from scipy import stats as st

LINEWIDTH = 35

def main() -> None:
    parser = argparse.ArgumentParser(description="Runs a set of tests using a STT framework")
    parser.add_argument("file")
    parser.add_argument("files", nargs="+")
    parser.add_argument("--metric", choices=["wer", "cer"], default="wer")
    parser.add_argument("--pvalue", choices=["significant", "value", "only"], default="significant")
    args = parser.parse_args()
    files = [args.file, *args.files]

    name_formater = lambda file: Path(file).stem
    if len({name_formater(x) for x in files }) < len(files):
        name_formater = lambda file: Path(file).parent.stem + "/" + Path(file).stem

    results = pd.DataFrame(
        columns=[ name_formater(x) for x in files ]
    )

    # Test the mean for every model pair
    print(f"Means\n{'='*LINEWIDTH}")
    for file in files:
        base = pd.read_json(file)
        a = base[args.metric].to_numpy()
        print(f"{name_formater(file):15}: {base[args.metric].mean():0.3f} : {base[args.metric].std():0.3f}")
        strings = []
        for target_file in files:
            if file == target_file:
                strings.append("")
            else:
                target = pd.read_json(target_file)
                b = target[args.metric].to_numpy()
                res = st.ttest_ind(a, b)
                if args.pvalue == "only":
                    strings.append(format_pvalue(res.pvalue))
                elif args.pvalue == "value":
                    strings.append(f"{res.statistic:.2f}({format_pvalue(res.pvalue)})")
                else:
                    strings.append(f"{res.statistic:.2f}{'*' if res.pvalue < 0.05 else ''}")
        results.loc[name_formater(file)] = strings
    print(f"\nResults\n{'='*LINEWIDTH}")
    print(results)



def format_pvalue(pvalue: float) -> str:
    if pvalue < 0.001: return "<0.001"
    else: return f'{pvalue:.3f}'

if __name__ == "__main__":
    main()
