
import sys
from typing import Dict

def eprint(*args, **kwargs) -> None:
    print(*args, file=sys.stderr, **kwargs)

def print_dictionary(dict: Dict) -> None:
    if not dict: return
    longest = max(map(lambda x: len(x), dict.keys()))
    for key, value in dict.items():
        print(f"{key:{longest}} : {value}")
