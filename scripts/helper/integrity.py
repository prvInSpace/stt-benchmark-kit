
import hashlib
from typing import List

def calculate_md5(*files: List[str]) -> str:
    hash_md5 = hashlib.md5()
    for file in files:
        with open(file, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
    return hash_md5.hexdigest()

