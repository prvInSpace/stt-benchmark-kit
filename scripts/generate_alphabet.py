#!/usr/bin/env python3

import argparse
import pandas as pd
from pathlib import Path

def main():
    parser = argparse.ArgumentParser("Generates an alphabet file based on the content in the Common Voice files")
    parser.add_argument("infile")
    parser.add_argument("outfile")
    args = parser.parse_args()
    
    df = pd.read_csv(args.infile, sep="\t")
    df['sentence'] = df['sentence'].str.lower()
    Path(args.outfile).write_text(
        "\n".join(
            sorted(set(' '.join(df['sentence'].values)))
        ) + "\n"
    )
    

if __name__ == "__main__":
    main()