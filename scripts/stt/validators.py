
class InvalidParameter(RuntimeError):
    def __init__(self, msg, *args: object) -> None:
        super().__init__(*args)
        self.message = msg

def int_validator(value: str, min=None, max=None):
    if value is not list:
        value = [ value ]

    for string in value:
        number = int(string)
        # Verify that the number is within the bounds
        if min is not None and number < min:
            raise InvalidParameter(f"Number {number} out of bounds. Minimum {min}")
        if max is not None and number > max:
            raise InvalidParameter(f"Number {number} out of bounds. Maximum {max}")
    
    return True


def float_validator(value: str, min=None, max=None):
    if not isinstance(value, list):
        value = [ value ]

    print(value)

    for string in value:
        try:
            number = float(string)
        except:
            raise InvalidParameter(f"Number {string} could not be converted to float")
        # Verify that the number is within the bounds
        if min is not None and number < min:
            raise InvalidParameter(f"Number {number} out of bounds. Minimum {min}")
        if max is not None and number > max:
            raise InvalidParameter(f"Number {number} out of bounds. Maximum {max}")
    
    return True


    