

from functools import partial
from stt.validators import InvalidParameter, float_validator
from stt.base import STTModule
from typing import Dict

class CoquiModule:

    VALID_PARAMETERS = {
        "learning_rate": partial(float_validator, min=0.00001, max=0.05)
    }

    def verify_parameters(self, params: Dict[str, str]) -> bool:
        errors = []
        for key, value in params.items():
            if key not in self.VALID_PARAMETERS:
                errors.append(f"Key {key!r} not a valid parameter")
                continue
            try:
                self.VALID_PARAMETERS[key](value)
            except InvalidParameter as e:
                errors.append(e.message)

        if errors:
            print('\n'.join(errors))
            return False
        
        return True


def get_module(*args, **kwargs) -> STTModule:
    return CoquiModule(*args, *kwargs)
