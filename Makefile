# =============================================================================
# Created By  : Preben Vangberg
# Copyright   : Copyright 2022
# License     : MIT
# Version     : 1.0.0
# Maintainer  : Preben Vangberg
# Email:      : prv21fgt@bangor.ac.uk
# Created     : 2022 October 24
# =============================================================================
# This is the main make file for the project. The default action is to download
# the Common Voice 11 datasets, created a merged version of these.
# =============================================================================

IMAGE_NAME := $(USER)/stt-benchmark
DOCKER_PARAMS := --shm-size=2g --ulimit memlock=-1 --ulimit stack=67108864 -it --rm

run:
	nvidia-docker run $(DOCKER_PARAMS) \
	   	-v ${PWD}/data/cv/$(LANG):/data/ \
		-v ${PWD}/$(MODEL):/results/ \
		-e "LANG=${LANG}" \
		--entrypoint bash \
		--name=${USER}-$(LANG) $(IMAGE_NAME)/${FRAMEWORK} \
		${COMMAND}




datasets: data/cv/rm-vallader/validated.tsv
	@echo $(FRAMEWORK)
	@echo "Succesfully downloaded all datasets"

# Default rule for any Common Voice dataset
data/cv/%/validated.tsv:
	mkdir -p data/cv
	$(eval LANG=$(shell echo $@ | cut -d/ -f3))
	wget -O cv_${LANG}.tar.gz https://mozilla-common-voice-datasets.s3.dualstack.us-west-2.amazonaws.com/cv-corpus-11.0-2022-09-21/cv-corpus-11.0-2022-09-21-${LANG}.tar.gz
	tar --full-time -xzf cv_${LANG}.tar.gz	
	rm -f cv_${LANG}.tar.gz
	mv cv-corpus-11.0-2022-09-21/${LANG} data/cv/
	rmdir cv-corpus-11.0-2022-09-21

# Builds the Docker image using the Dockerfile in the folder.
# The .DOCKER-IMAGE file is used to keep track of when the
# last image was built to prevent excessive rebuilding.
build: .DOCKER_IMAGE/$(FRAMEWORK)
.DOCKER_IMAGE/%: $(shell find stt-scripts/$(FRAMEWORK))
	@echo $^
	$(eval ASR_FRAMEWORK=$(shell echo $@ | cut -d/ -f2))
	docker build -t "$(IMAGE_NAME)/${ASR_FRAMEWORK}" stt-scripts/$(ASR_FRAMEWORK)/ 
	@mkdir -p .DOCKER_IMAGE
	@touch $@
