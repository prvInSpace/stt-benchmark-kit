#!/usr/bin/bash

export CUDA_VISIBLE_DEVICES=0

if [ ! -f /data/clips/validated.csv ]; then
	bin/import_cv2.py --filter_alphabet /data/alphabet.txt /data
fi

if [ ! -f /results/checkpoints ]; then
	mkdir -p /results/checkpoints
fi

python3 /scripts/train.py

touch finished
