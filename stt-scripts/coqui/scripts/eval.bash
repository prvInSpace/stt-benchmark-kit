export CUDA_VISIBLE_DEVICES=0

python3 -m coqui_stt_training.evaluate \
		--use_allow_growth true \
		--test_files /data/clips/test.csv \
		--alphabet_config_path /data/alphabet.txt \
		--test_batch_size 16 \
        --test_output_file /results/results.json \
		--checkpoint_dir /results/checkpoints 

