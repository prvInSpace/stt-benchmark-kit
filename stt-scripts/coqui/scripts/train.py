
import functools
import json
import subprocess
from typing import Dict

def main():
    params = load_params()
    params = functools.reduce(
        lambda acc, val: acc + [*val],
        params.items(),
        []
    )
    print(' '.join(["python3", "-m", "coqui_stt_training.train", *params]))
    rc = subprocess.run(["python3", "-m", "coqui_stt_training.train", *params])
    

def load_params() -> Dict[str, str]:
    params = {
        # Used to get it to run properly
        "--load_cudnn": "true",
	    "--use_allow_growth": "true",
     
        # Training and dev files
        "--dev_files": "/data/clips/dev.csv",
	    "--train_files": "/data/clips/train.csv",
	#    "--test_files": "/data/clips/train.csv",
        "--alphabet_config_path": "/data/alphabet.txt",
     
        # Batch sizes
	    "--train_batch_size": "16",
	    "--dev_batch_size": "16",
     
        # Checkpoint
        "--checkpoint_dir": "/results/checkpoints",
        
        # Learning parameters
#	    "--learning_rate": "0.001",
#	    "--dropout_rate": "0.2",
#	    "--reduce_lr_on_plateau": "true",
#        "--drop_source_layers": "2",
	    "--epochs": "50",
        
        # Misc
	    "--max_to_keep": "1"
    }
    with open("/results/hyperparams.json") as infile:
        param_overrides = json.load(infile)
        for key, value in param_overrides.items():
            print(key, value)
            params["--" + key] = value
    return params
        

if __name__ == "__main__":
    main()
